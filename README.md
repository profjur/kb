# **Инструкция по подготовке статей для Базы знаний** 

Важно соблюдать эти рекомендации, чтобы процесс подготовки материалов был эффективным. Сложность может быть в том, что статью нужно оформить с разметкой 
маркдаун. Но на самом деле разобраться в этом можно в два счёта, а в копилку — владение новым инструментом. 

* [Регистрация на Gitlab и настройка интерфейса](#регистрация-на-gitlab-и-настройка-интерфейса)
* [Создание статьи](#создание-статьи)
* [Редактирование статьи](#редактирование-статьи)
* [Структура статьи и разметка Markdown](#структура-статьи-и-разметка-markdown)

# Регистрация на Gitlab и настройка интерфейса

Для начала вам надо [зарегистрироваться на Gitlab](https://gitlab.com/users/sign_in#register-pane). После регистрации вы можете [настроить свой профиль](https://gitlab.com/profile), например, установить аватар и добавить контактные данные.

Теперь вы готовы создать или отредактировать статью в нашей базе знаний!

# Создание статьи

У нашей Базы знаний довольно простая структура: отдельные статьи лежат в репозитории (хранилище) в виде отдельных файлов формата Markdown. Специальный скрипт автоматически собирает из файлов со статьями симпатичный сайт.

Процесс создания новой статьи выглядит следующим образом:

1. Зайдите в раздел [Issues](https://gitlab.com/profjur/kb/issues) (в меню в левой части экрана). Посмотрите, нет ли там темы, которую вы собираетесь предложить. Если такая тема уже есть – проверьте, может быть нужны авторы. Если темы ещё нет – нажмите кнопку New Issue и заполните сообщение по предложенному шаблону. Когда вы создаёте, или комментируете тему – вы автоматически подписываетесь на оповещения о новых сообщениях в теме. Это удобно, чтобы не упускать обсуждение. 

1. На главной странице проекта есть две папки: папка [_journalistsrights](/_journalistsrights) и папка [_freesoftware](_freesoftware). Это, соответственно, папки для статей, посвящённых правам журналистов и Free Software. Зайдите в нужную папку и нажмите на значок + в левой верхней части, выберите New File: ![](_media/tutorial-1.png)
Перед вами откроется окно редактирования текста. Теперь вы можете написать свою статью прямо здесь, либо вставить уже готовый текст из Markdown-редактора. Не забудьте назвать свой файл понятным именем, например: `otpusk.md` или `vacation.md`:<br> ![](_media/tutorial-2.png)
<br>Когда вы закончили редактирование статьи, проверьте всё ещё раз и нажмите на зелёную кнопку Commit Changes внизу файла. В поле Commit message вы можете написать комментарий к новой статье (по желанию). Если вы хотите, чтобы вас упомянули на странице с авторами базы знаний, напишите в этом поле своё имя или псевдоним: ![](_media/tutorial-3.png)

1. Остался последний шаг. Перед вами откроется окно Merge Request (запроса на слияние), который необходимо отправить, чтобы статья попала на рассмотрение к редакторам и после появилась в базе знаний. Поля на этой странице заполнять не обязательно. Нажмите Submit merge request внизу страницы, чтобы статья отправилась к нам: ![](_media/tutorial-4.png)
<br>Ура! Теперь надо немного подождать, пока мы рассмотрим статью и выложим её в базу знаний. Для удобства вы можете зайти в Issue, которое вы создали в пункте 1. и написать в комментариях название нового файла.

# Редактирование статьи

Предположим, вы хотите отредактировать уже существующую статью (свою или чужую).

1. Откройте нужный файл и нажмите Edit в правой части экрана:![](_media/tutorial-5.png)
Внесите все необходимые изменения.

1. После этого нажмите Зелёную кнопку Commit Changes внизу экрана:![](_media/tutorial-6.png)

1. Третий шаг аналогичен шагу 3 из раздела Создание статьи.

# Структура статьи и разметка Markdown

В нашей базе знаний мы используем язык разметки Markdown, это удобный инструмент для быстрого и красивого оформления текста. [Вот здесь](https://gist.github.com/Jekins/2bf2d0638163f1294637) есть неплохое руководство по Markdown, самые важные примеры мы приведём и в этой статье.

Писать текст в разметке Markdown можно прямо на Gitlab, или в отдельном редакторе. Мы рекомендуем редактор [StackEdit](https://stackedit.io/), это open-source редактор, который работает прямо у вас в браузере, устанавливать ничего не потребуется.

Не рекомендуем переносить текст напрямую из редакторов типа Word или Google Docs, по крайней мере лучше сначала вставить текст в StackEdit, чтобы удостовериться, что форматирование правильное.

# Заголовок статьи
Заголовок должен быть максимально информативным. Можно сделать заголовок из двух частей, как в примере ниже, но постарайтесь не перегружать его, задача в том, чтобы людям было максимально просто и удобно найти информацию.

Сделать заголовок в Markdown очень просто:
```
# Задержки зарплат и гонораров. Какую ответственность несет издание
```
Отобразится как
# Задержки зарплат и гонораров. Какую ответственность несет издание

# ЛИД

Абзац из 3-5 предложений о сути материала. Не используйте общие фразы, вроде «Удержание гонораров — одна из частных проблем, с которыми сталкиваются журналисты» или «Все знают, что у журналистов невысокие зарплаты, так они приходят еще и не вовремя». Если у вас есть данные исследований или другая фактическая информация, которая подтверждает проблематику вопроса, их в ЛИДе использовать можно. 

# Основной текст 
Объем текста может быть от 3,5 до 12 тыс знаков. Главное, чтобы он отвечал на главный вопрос текста и был понятным для читателя. 

Текст нужно разбить на логические разделы и обозначить их подзаголовками. Для подзаголовков мы используем заголовки второго уровня.
```
## Подзаголовок
```
Отобразится как

## Подзаголовок

Текст можно сделать жирным, чтобы выделить значимые места.ё

```
**жирный текст**
```

отобразится как:

**жирный текст**

Обратите внимание, что мы используем букву **ё.**

В текст можно вставить ссылку:

```
[Профсоюз журналистов](https://profjur.org/)
```

Отобразится как:

[Профсоюз журналистов](https://profjur.org/)

В принципе, ссылку можно просто скопировать и вставить в текст, она автоматически преобразуется в кликабельную:
````
https://profjur.org
````
https://profjur.org

А можно – картинку (с помощью ссылки на картинку):

```
![](_media/tutorial-1.png)
```

Отобразится как:

![](_media/tutorial-1.png)

Подвёрстка – это необязательный элемент текста, который можно использовать для того, чтобы выделить в тексте отдельные части, например, реальные кейсы и истории по теме.

Подвёрстку мы делаем вот так:
<pre>
``` 
Подвёрстка 
```
</pre>

Отобразится как:

``` 
Подвёрстка 
```

Бонус для тех, кто дочитал до конца.

Почему мы решили со всем этим заморочиться?

* Во-первых, так мы не зависим ни от какой платформы и системы для публикации. Markdown-файлы очень легко переместить в другую систему, если у нас будет такая надобность.
* Во-вторых, каждый может подключиться и вносить дополнения и исправления – нет надобности придумывать специальные инструменты для совместного редактирования.
* В-третьих, держать источники в открытом виде – это модно и современно.

Супер-бонус: почему не Github, а Gitlab?

* С недавних пор Github принадлежит корпорации Microsoft, а мы предпочитаем обходиться без корпораций там, где это возможно.